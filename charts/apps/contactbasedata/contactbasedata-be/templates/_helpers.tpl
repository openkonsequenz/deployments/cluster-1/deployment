{{/*
Expand the name of the chart.
*/}}
{{- define "contactbasedata-be.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Lookup is not yet supported in ArgoCD
*/}}
{{- define "lookup-postgres-shared-db-image" -}}
{{- $sharedEnvironment := .Values.sharedEnvironment }}
{{- range $index, $service := (lookup "v1" "Pod" $sharedEnvironment "postgresql-sfs-0").spec.containers }}
    {{- $service.image }}
{{- end }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "contactbasedata-be.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "contactbasedata-be.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "contactbasedata-be.labels" -}}
helm.sh/chart: {{ include "contactbasedata-be.chart" . }}
{{ include "contactbasedata-be.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "contactbasedata-be.selectorLabels" -}}
app.kubernetes.io/name: {{ include "contactbasedata-be.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "contactbasedata-be.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "contactbasedata-be.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}
