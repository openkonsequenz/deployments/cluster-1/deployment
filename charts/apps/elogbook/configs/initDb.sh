#!/bin/bash

echo Waiting for DB to be ready...;
while ! pg_isready >dev/null; do sleep 3; done;
echo DB is ready and accepting conncetions;
sleep 3;
{{ if .Values.resetDBonDeploy }}
echo ------------------------------------------------------------------------;
echo Dropping active connections to database ${APP_DBNAME};
psql -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = '${APP_DBNAME}' AND pid <> pg_backend_pid()";
echo Dropping database ${APP_DBNAME} if exists;
psql -c "DROP DATABASE IF EXISTS \"${APP_DBNAME}\"";
echo Dropping user ${APP_DB_ROLE} if exists;
psql -c "DROP USER IF EXISTS \"${APP_DB_ROLE}\"";
{{ end }}
echo ------------------------------------------------------------------------;
echo Creating role ${APP_DB_ROLE} if not exists;
psql -tc "SELECT 1 FROM pg_user WHERE usename = '${APP_DB_ROLE}'" | grep -q 1 || psql -c "CREATE USER \"${APP_DB_ROLE}\" WITH PASSWORD '${APP_DB_ROLE}'";
echo Creating database ${APP_DBNAME} if not exists;
psql -tc "SELECT 1 FROM pg_database WHERE datname = '${APP_DBNAME}'" | grep -q 1 || psql -U postgres -c "CREATE DATABASE \"${APP_DBNAME}\"";
echo ------------------------------------------------------------------------;
echo Creating tables on database ${APP_DBNAME} if not exits;
PGPASSWORD=${APP_DB_ROLE};
tabelCount=$(psql -U ${APP_DB_ROLE} -d ${APP_DBNAME} -tc "select count(*) from information_schema.tables where table_schema = 'public'")
tabelCount=`echo $tabelCount`
echo tabelCount: $tabelCount
if [[ $tabelCount == "0" ]]; then
    echo Creating tables on database ${APP_DBNAME};
    echo ------------------------------------------------------------------------;
    PGPASSWORD=${APP_DB_PASSWORD};
    /docker-entrypoint-initdb.d/02_create_DB_2.0.0.sh;
    echo Insert data into tables on database ${APP_DBNAME};
    echo ------------------------------------------------------------------------;
    /docker-entrypoint-initdb.d/03_config_DB_2.0.0_Demo.sh;
    echo Migrate from 2.0.0 to 2.0.1;
    echo ------------------------------------------------------------------------;
    /docker-entrypoint-initdb.d/01_migrate_from_2.0.0_to_2.0.1.sh;
else
    echo "Data already exists, create and insert scripts are not executed."
fi